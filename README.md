# Updated (2024-01-01) PKGBUILD for InteractiveBrokers Trader Workstation "latest"

I built this as a one-off because I wanted to try the package and the one on [AUR](https://aur.archlinux.org/packages/tws-latest) was out-of-date and non-functional.  

I don't really plan to continuously maintain this, but as of 2024-01-01, this PKGBUILD works.

Feel free to open an issue here if you're using this and need assistance.

- fauxmight